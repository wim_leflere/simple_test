'''
Some basic tests for testing a simple project on Jenkins

@author: Wim Leflere
'''
import time
import pytest

test_delay = 2

def setup_function(function):
    time.sleep(test_delay)

def test_true():
    assert True

def test_false():
    assert False == False

def test_equal():
    assert 1 == 1

def test_not_equal():
    assert 1 != 2

def test_in():
    a = [1, 2, 3, 4, 5]
    assert 1 in a

def test_not_in():
    a = [1, 2, 3, 4, 5]
    assert 6 not in a

def test_substring():
    assert 'nan' in 'banana'

def test_sum():
    assert 2 == (1 + 1)

@pytest.mark.skipif(True, reason='skip test')
def test_skip():
    assert False
